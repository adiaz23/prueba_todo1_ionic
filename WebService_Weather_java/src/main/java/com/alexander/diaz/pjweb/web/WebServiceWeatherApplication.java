package com.alexander.diaz.pjweb.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebServiceWeatherApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebServiceWeatherApplication.class, args);
	}

}
