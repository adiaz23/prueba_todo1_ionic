/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alexander.diaz.pjweb.web.rest;

import com.alexander.diaz.pjweb.web.dto.Result;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import javax.net.ssl.SSLContext;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author jadiaz
 */

@RestController
@CrossOrigin(origins = "*")
public class WeatherController {
    
    private static String URL_WEATHER = "https://api.darksky.net/forecast/edeec6ab2fc01009f4db1fb4c14fb6ef/";
    
    private static String URL_ADDRESS = "https://maps.googleapis.com/maps/api/geocode/json?sensor=true&key=AIzaSyDGtPFNxPoW78TlKzKf_VLw8WrjqNtZwOQ&latlng=";
    
    @GetMapping("/getInformation")
    public Result getInformation(
           
            @RequestParam(value="latitude") String latitude,
            @RequestParam(value="longitude") String longitude
    ){
        
        String urlWeather = URL_WEATHER+latitude+","+longitude;
        
        Result result = new Result();
        
        String resultado = "";
        
        try {
            
            TrustStrategy acceptingTrustStrategy;
            acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;

            SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
                    .loadTrustMaterial(null, acceptingTrustStrategy)
                    .build();

            SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);

            CloseableHttpClient httpClient = HttpClients.custom()
                    .setSSLSocketFactory(csf)
                    .build();

            HttpComponentsClientHttpRequestFactory requestFactory =
                    new HttpComponentsClientHttpRequestFactory();

            requestFactory.setHttpClient(httpClient);

            RestTemplate restTemplate = new RestTemplate(requestFactory);

            resultado = restTemplate.getForObject(urlWeather, String.class);
        } catch (KeyManagementException | KeyStoreException | NoSuchAlgorithmException | RestClientException e) {
            System.err.println("Error: "+e.getMessage());
        }
        
        result.setData(resultado);
        result.setResult(true);
        result.setMessage("");
        result.setError("");

        
        return result;
    }
    @GetMapping("/getGeodeo")
    public Result getGeodeo(
            @RequestParam(value="latitude") String latitude,
            @RequestParam(value="longitude") String longitude){
        
        String urlAddress = URL_ADDRESS+latitude+","+longitude;
        
        Result result = new Result();
        
        String resultado = "";
        
        try {
            
            TrustStrategy acceptingTrustStrategy;
            acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;

            SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
                    .loadTrustMaterial(null, acceptingTrustStrategy)
                    .build();

            SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);

            CloseableHttpClient httpClient = HttpClients.custom()
                    .setSSLSocketFactory(csf)
                    .build();

            HttpComponentsClientHttpRequestFactory requestFactory =
                    new HttpComponentsClientHttpRequestFactory();

            requestFactory.setHttpClient(httpClient);

            RestTemplate restTemplate = new RestTemplate(requestFactory);

            resultado = restTemplate.getForObject(urlAddress, String.class);
        } catch (KeyManagementException | KeyStoreException | NoSuchAlgorithmException | RestClientException e) {
            System.err.println("Error: "+e.getMessage());
        }
        
        result.setData(resultado);
        result.setResult(true);
        result.setMessage("");
        result.setError("");

        return result;
        
    }
}
