import { Injectable } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  private latitude:any;
  private longitude:any;

  constructor(private geolocation: Geolocation,private http: HttpClient ) {

   }

   getLocation(){
    return this.geolocation.getCurrentPosition().then((resp) => {
      console.log(resp);
      this.latitude = resp.coords.latitude;
      this.longitude =  resp.coords.longitude;
      return resp;
    }).catch((error) => {
      console.log('Error getting location', error);
      return error;
    });
   }


  getWeather(lat: any, log: any){

    const headers = new HttpHeaders({
      'Access-Control-Allow-Origin':'*'
    });

    return this.http.get(`http://localhost:8080/getInformation?latitude=${lat}&longitude=${log}`, {headers});
  }

  getAddress(lat: any, log: any){

    const headers = new HttpHeaders({
      'Access-Control-Allow-Origin':'*'
    });

    return this.http.get(`http://localhost:8080/getGeodeo?latitude=${lat}&longitude=${log}`, {headers});

  }
}
