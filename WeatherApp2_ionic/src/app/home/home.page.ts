import { Component } from '@angular/core';
import { WeatherService } from '../services/weather.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  coordinates: any;
  weather: any;
  address: any;
  locationName: any;
  temperature: number;
  summary: any;
  precipProbability: number;
  humidity: number;
  icon: any;

  constructor(private weatherService: WeatherService) {
    this.updateWeather();
  }

  updateWeather(){
    this.coordinates = this.weatherService.getLocation();
    this.coordinates
    .then( (data: any) => {
      //console.dir(data);
      this.weatherService.getWeather(data.coords.latitude, data.coords.longitude).subscribe(
        (res: any) => {
          this.weather = JSON.parse(res.data);
          console.log(this.weather);

          this.temperature = this.weather.currently.temperature ;
          this.temperature = ((this.temperature-32) * 5)/9;
          this.summary = this.weather.currently.summary;
          this.precipProbability = this.weather.currently.precipProbability * 100;
          this.humidity = this.weather.currently.humidity * 100;
          this.icon = this.weather.currently.icon;
        },
        (error: any) => {
          console.error(error);
        }
      );

      this.weatherService.getAddress(data.coords.latitude, data.coords.longitude).subscribe(
        (res: any) => {
          this.address = JSON.parse(res.data);
          let [, r1, r2, r3] = this.address.results[0].formatted_address.split(',');
          this.locationName = `${r1}, ${r2}, ${r3}`;
          console.log(this.locationName);
        },
        (error: any) => {
          console.error(error);
        }
      );

    })
    .catch((error: any) => {
      console.log("Error");
      console.log(error);
    });
  }
}