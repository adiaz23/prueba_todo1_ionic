var URL_WEATHER = "https://api.darksky.net/forecast/edeec6ab2fc01009f4db1fb4c14fb6ef/";
var URL_WEATHER_ICON = "https://darksky.net/images/weather-icons/";
var URL_ADDRESS = "https://maps.googleapis.com/maps/api/geocode/json?sensor=true&key=AIzaSyDGtPFNxPoW78TlKzKf_VLw8WrjqNtZwOQ&latlng=";
var context;
var app = {

    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {


        //Instancia de plugin para Permisos
        var permissions = cordova.plugins.permissions;
        context = this;

        //Comprobar si es Android
        if(window.cordova.platformId == "android"){

            //Lista de permisos a checker/solicitar
            var list = [
                permissions.ACCESS_COARSE_LOCATION,
                permissions.ACCESS_FINE_LOCATION,
                permissions.INTERNET,
                permissions.ACCESS_NETWORK_STATE
            ];

            //Checkea permisos
            permissions.checkPermission(list, function( status ){
                //Si tiene permisos
                if ( status.hasPermission ) {
                    
                    context.getLocationInfo();//Obtiene toda la informacion de clima y ubicacion
                    
                    //Crea evento de refrescar para obtener informacion
                    $('#refresh').click(function() {
                        context.getLocationInfo();
                    });
                }else {//Si no tiene permisos
                    
                    //Solicita los permisos
                    permissions.requestPermissions(list, function( status ){
                        
                        //Si tiene permisos
                        if ( status.hasPermission ) {
                            
                            context.getLocationInfo();//Obtiene toda la informacion de clima y ubicacion
                            
                            //Crea evento de refrescar para obtener informacion
                            $('#refresh').click(function() {
                                context.getLocationInfo();
                            });
                        }
                    });

                }
            });
        }else{//Si es web/diferente a Android
            
            context.getLocationInfo();//Obtiene toda la informacion de clima y ubicacion
                    
            //Crea evento de refrescar para obtener informacion
            $('#refresh').click(function() {
                context.getLocationInfo();
            });
        }
    },

    getLocationInfo: function() {
        //Hace uso de geolocation para obtener Latitud y Longitud
        navigator.geolocation.getCurrentPosition(this.onSuccessGeo, this.onErrorGeo);

    },

    //Funcion que activa o desactiva el efecto de nubes
    fxCloud: function(active){
        if(active){
            $('.clouds').show();
        }else{
            $('.clouds').hide();
        }
    },

    //Funcion que activa o desactiva el efecto de lluvia
    fxRain: function(active){
        makeItRain();
        if(active){
            $('.rainFx').show();
            $('body').addClass('nightColor');
        }else{
            $('.rainFx').hide();
            $('body').removeClass('nightColor');
        }
    },

    //Metodo exitoso de geolocation donde extrae Lat y Lng
    onSuccessGeo: function(position) {
        //Realiza una peticion GET al Api de Clima
        $.ajax({
            type: "GET",
            url: URL_WEATHER + position.coords.latitude + "," + position.coords.longitude,
            success: function(data) {
                //Respuesta exitosa de la peticion

                //Valor de humedad
                var humidity = Math.floor(data.currently.humidity * 100);
                $('#detail_2').html(humidity);

                //Tipo de precipitaciones
                $('#detail_3').html(data.currently.precipType);

                //Valor de precipitaciones
                var precipProbability = Math.floor(data.currently.precipProbability * 100);
                $('#detail_4').html(precipProbability);

                //Icono del clima
                $('#imageWeather').attr("src", URL_WEATHER_ICON + data.currently.icon + ".png");

                //Dependiendo del icono activa/desactiva los efectos
                //Lluia, nubes, dia, noche
                switch (data.currently.icon){
                    case "clear-day":
                            context.fxCloud(false);
                            context.fxRain(false);
                            $('body').removeClass('nightColor');
                        break;
                    case "clear-night":
                            context.fxCloud(false);
                            context.fxRain(false);
                            $('body').addClass('nightColor');
                        break;
                    case "rain":
                            context.fxRain(true);
                            context.fxCloud(false);
                        break;
                    case "snow":
                    case "wind":
                    case "sleet":
                        break;
                    case "fog":
                    case "cloudy":
                            context.fxCloud(true);
                            context.fxRain(false);
                        break;
                    case "partly-cloudy-day":
                            context.fxCloud(true);
                            context.fxRain(false);
                            $('body').removeClass('nightColor');
                        break;
                    case "partly-cloudy-night":
                            context.fxCloud(true);
                            context.fxRain(false);
                            $('body').addClass('nightColor');
                        break;
                    default:
                            context.fxCloud(false);
                            context.fxRain(false);
                            $('body').removeClass('nightColor');
                        break;

                }

                //Obtiene la informacion de la temperatura 
                //Convierte de fahrenheit a celius
                var fahrenheit = data.currently.temperature;
                var celius = (fahrenheit - 32) * 5 / 9;
                $('#tempValue').html(Math.floor(celius));

                        
                //Dependiendo de la hora activa dia o noche
                var d = new Date();
                var n = d.getHours();

                if(  (n >= 18 && n <= 23) || (n >= 0 && n <= 6) ){
                    $('body').addClass('nightColor');
                }else{
                    $('body').removeClass('nightColor');
                }
            }
        });

        //Hace uso del Api d Google Maps
        var geocoder = new google.maps.Geocoder;
        var latlng = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
        };

        //Obtiene la informacion de la ubicacion
        //Mediante Lat/Lng
        //Itera los nodos de Address_Components
        //Obtiene Ciudad, Departamento y Pais
        geocoder.geocode({
            'location': latlng
        }, function(results, status) {
            var arrayAddress = ["..", "..", ".."];
            if (status === 'OK') {
                if (results[0] && results[0].address_components.length > 0) {
                    for (i = 0; i < results[0].address_components.length; i++) {
                        var dataComponent = results[0].address_components[i];
                        for (j = 0; j < dataComponent.types.length; j++) {
                            var type = dataComponent.types[j];

                            //Obtiene Ciudad, Departamento y Pais
                            switch (type) {
                                case "administrative_area_level_2":
                                    arrayAddress[0] = dataComponent.short_name;
                                    break;
                                case "administrative_area_level_1":
                                    arrayAddress[1] = dataComponent.short_name;
                                    break;
                                case "country":
                                    arrayAddress[2] = dataComponent.short_name;
                                    break;
                            }
                        }
                    }
                }
            }
            //Pinta Ciudad, Departamento y Pais
            $('#locationName').html(arrayAddress.toString());
        });

    },

    onErrorGeo: function(position) {}

};

app.initialize();