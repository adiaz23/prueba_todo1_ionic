/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */


var URL_WEATHER = "https://api.darksky.net/forecast/edeec6ab2fc01009f4db1fb4c14fb6ef/";
var URL_WEATHER_ICON = "https://darksky.net/images/weather-icons/";
var URL_ADDRESS = "https://maps.googleapis.com/maps/api/geocode/json?sensor=true&key=AIzaSyDGtPFNxPoW78TlKzKf_VLw8WrjqNtZwOQ&latlng=";
var app = {

    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var context = this;
        context.getLocationInfo();
        $('#refresh').click(function() {
            context.getLocationInfo();
        });
    },

    getLocationInfo: function() {
        navigator.geolocation.getCurrentPosition(this.onSuccessGeo, this.onErrorGeo);

    },

    onSuccessGeo: function(position) {
        $.ajax({
            type: "GET",
            dataType: "jsonp",
            url: URL_WEATHER + position.coords.latitude + "," + position.coords.longitude,
            success: function(data) {
                var humidity = Math.floor(data.currently.humidity * 100);
                $('#detail_2').html(humidity);
                $('#detail_3').html(data.currently.precipType);
                var precipProbability = Math.floor(data.currently.precipProbability * 100);
                $('#detail_4').html(precipProbability);
                $('#imageWeather').attr("src", URL_WEATHER_ICON + data.currently.icon + ".png");

                var fahrenheit = data.currently.temperature;
                var celius = (fahrenheit - 32) * 5 / 9;
                $('#tempValue').html(Math.floor(celius));
            }
        });

        var geocoder = new google.maps.Geocoder;
        var latlng = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
        };

        geocoder.geocode({
            'location': latlng
        }, function(results, status) {
            var arrayAddress = ["xxxx", "xxx xx xxxx", "xx"];
            if (status === 'OK') {
                if (results[0] && results[0].address_components.length > 0) {
                    for (i = 0; i < results[0].address_components.length; i++) {
                        var dataComponent = results[0].address_components[i];
                        for (j = 0; j < dataComponent.types.length; j++) {
                            var type = dataComponent.types[j];
                            switch (type) {
                                case "administrative_area_level_2":
                                    arrayAddress[0] = dataComponent.short_name;
                                    break;
                                case "administrative_area_level_1":
                                    arrayAddress[1] = dataComponent.short_name;
                                    break;
                                case "country":
                                    arrayAddress[2] = dataComponent.short_name;
                                    break;
                            }
                        }
                    }
                }
            }
            $('#locationName').html(arrayAddress.toString());
        });

    },

    onErrorGeo: function(position) {}

};

app.initialize();